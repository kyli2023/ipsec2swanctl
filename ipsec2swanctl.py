#! /bin/python3 -B

"""
Script to convert ipsec.conf with ipsec.secrets into swanctl.conf file format
Written by Noel Kuntze for Sysmex Europe GmbH
Licensed under the GPLv3
"""

import argparse
import collections
import copy
import glob
import logging
import os
import sys

class Ipsec2Swanctl:
    """
    class for transforming ipsec.conf format to swanctl.conf
    """

    auth_dict = {
        "leftid" : "local-0.id",
        "rightid" : "remote-0.id",
        "leftid2" : "local-1.id",
        "rightid2" : "remote-1.id",
        "leftauth" : "local-0.auth",
        "rightauth" : "remote-0.auth",
        "leftauth2" : "local-1.auth",
        "rightauth2" : "remote-1.auth",
        "leftcert" : "local-0.certs",
        "rightcert" : "remote-0.certs",
        "leftcert2" : "local-1.certs",
        "rightcert2" : "remote-1.certs",        
    }


    value_map = {
        "add" : "none",
        "route" : "trap",
        "start" : "start"
    }

    class AuthSection():
        families = {
            "local-0" : True,
            "local-1" : True,
            "remote-0" : True,
            "remote-1" : True,
        }
        def __init__(self, auth_settings):
            translated = {}
            for key, value in auth_settings.items():
                item = Ipsec2Swanctl.auth_dict.get(key)
                if item:
                    translated[item] = value
            self.auth_settings = translated

        def to_swanctl(self):
            ret = ""
            sorted_keys = sorted(self.auth_settings)
            open_section=None
            for key in sorted_keys:
                splits = key.split(".")
                section = splits[0]
                if open_section != section:
                    if open_section != None:
                        ret += "}\n"
                    ret += "%s {\n" % section
                    open_section = section
                ret += "\t%s = %s\n" % (splits[1], self.auth_settings[key])
            if open_section:
                ret += "}"
            return ret


            # Complete this


    class Pool():
        def __init__(self, subnet, dns):
            self.subnet = ",".join(subnet)
            self.dns = ",".join(dns)

        def get_identifier(self):
            return "pool-subnet-%s" % self.subnet.replace(",", "-").replace(".", "-").replace(":", "-")

        def to_swanctl(self):
            ret ="%s {\n" % self.get_identifier()
            if self.subnet:
                ret += "\taddrs=%s\n" % self.subnet
            if self.dns:
                ret += "\tdns=%s\n" % self.dns
            ret += "}\n"
            return ret

    class Secret():
        valid_types = ["psk", "eap", "ike", "private", "rsa", "ecdsa", "pkcs8", "pkcs12"]
        legacy2new = {
            "psk" : "ike",
            "xauth" : "eap",
        }
        def __init__(self, selectors, secrets_type, rest):
            if isinstance(selectors, str):
                self.selectors = selectors.split(" ")
            else:
                self.selectors = list(selectors)
            secrets_type_lower = secrets_type.lower()
            new_type = self.legacy2new.get(secrets_type_lower)
            if new_type:
                secrets_type_lower = new_type
            if secrets_type_lower in self.valid_types:
                self.secrets_type = secrets_type_lower
            else:
                raise ValueError("Invalid secrets_type %s" % secrets_type_lower)
            self.rest = rest
            logging.debug("Generated new secret: %s" % self)

        def __repr__(self):
            return "%%IPsec secret selector %s type %s rest %s%% " % (
                self.selectors, self.secrets_type, self.rest)

        def generate_suffix(self):
            return "%s" % "".join(self.selectors).replace(".", "-").replace(":", "-").replace("#", "")

        def to_swanctl(self, suffix=None):
            """
            Converts the object into a string that is a valid swanctl secret
            """
            result = None
            rest_splits = self.rest.split(" ")
            result = "%s-%s {\n" % (self.secrets_type, self.generate_suffix())
            # add other data as specific to secrets type and parse self.rest accordingly
            if self.secrets_type in ("eap", "ike"):
                result += "\tsecret = %s\n" % rest_splits[0]
                for index, selector in enumerate(self.selectors):
                    if selector != "":
                        result += "\tid-%s=%s\n" % (index, selector)
            elif self.secrets_type in ("private", "rsa", "ecdsa", "pkcs8", "pkcs12"):
                result += "\tfilename=%s\n" % rest_splits[0]
            result += "}\n"
            return result

    class Connection():
        settings_map_reauth = {
            "no" : "0s"
        }
        settings_map_rekey = {
            "no" : "0s"
        }
        settings_map_keyexchange = {
            "ikev1": "1",
            "ikev2" : "2"
        }
        settings_map_pull = {
            "pull" : "yes",
            "push" : "no"
        }

        settings_map = {
            "reauth" : ("reauth_time", settings_map_reauth),
            "rekey" : ("rekey_time", settings_map_rekey),
            "keylife" : ("rekey_time", None),
            "ikelifetime" : ("rekey_time", None),
            "keyexchange" : ("version", settings_map_keyexchange),
            "right" : ("remote_addrs", None),
            "left" : ("local_addrs", None),
            "modeconfig" : ("pull", settings_map_pull),
            "leftsourceip" : ("vips", None),
            "leftsendcert" : ("send_cert", None),
            "aggressive" : ("aggressive", None),
            "ike" : ("proposals", None),
            "rightsourceip" : ("pools", None),
            "forceencaps" : ("encap", None),
            "leftikeport" : ("local_port", None),
            "rightikeport" : ("remote_port", None),
            "dpdtimeout": ("dpd_timeout", None),
            "dpddelay" : ("dpd_delay", None),
            "margintime" : ("margin_time", None),
            # Can not be translated properly yet
            # "rekeyfuzz" : ("rand_time", None),
            "mobike" : ("mobike", None),
        }

        def __init__(self, name: str, settings: dict, children: dict):
            self.name = name
            self.settings = copy.deepcopy(settings)
            self.children = children
            self.auth_section = None
            self.ignored = False
            self.transform_proposals()
            self.translate_settings()

        def __repr__(self):
            return self.to_swanctl()

        def to_swanctl(self, ike_sa_defaults_path=None, child_sa_defaults_path=None):
            ret ="%s {\n" % self.name
            if ike_sa_defaults_path:
                ret += "\tinclude %s\n" % ike_sa_defaults_path
            for key, val in self.settings.items():
                ret += "\t%s=%s\n" % (key, val)
            ret += "\tchildren {\n"
            for child in self.children.values():
                for line in child.to_swanctl(defaults_path=child_sa_defaults_path).split("\n"):
                    ret += "\t\t%s\n" % line
                # ret += "\t\t%s {\n" % name
                # for key, val in child.settings.items():
                #     ret += "\t\t\t%s=%s\n" % (key, val)
                # ret += "\t\t}\n"
            ret +="\t}\n"
            if self.auth_section:
                for line in self.auth_section.to_swanctl().split("\n"):
                    ret += "\t%s\n" % line
            ret += "}\n"
            return ret

        def get_client(self):
            """
            This method returns which side is local.
            This is a stub at the moment. It always returns "left".
            In order to replicate the behaviour of charon,
            checking if left or right is set to a local IP would be needed,
            and then defaulting to left.
            """
            return "left"

        def transform_proposals(self):
            for item in ("ike", "proposals"):
                existing_val = self.settings.get(item)
                if existing_val:
                    existing_val = existing_val.strip()
                    if not existing_val.endswith("!"):
                        existing_val = existing_val + ", default"
                    self.settings[item] = existing_val

        @classmethod
        def convert_settings(cls, settings):

            to_replace = {}
            for key, val in cls.settings_map.items():
                existing_val = settings.get(key)
                if existing_val:
                    to_replace[key] = val[0]
                    if val[1]:
                        translation_value = val[1].get(existing_val)
                        if translation_value:
                            settings[key] = translation_value
            for key, val in to_replace.items():
                settings[val] = settings[key]
                del settings[key]

        def translate_settings(self):
            to_set = []
            self.convert_settings(self.settings)
            for key, value in self.settings.items():
                if key == "auto" and value != "ignore":
                    self.ignored = False
                translated = Ipsec2Swanctl.auth_dict.get(key)
                if translated:
                    to_set.append((translated, value))

                    #self.settings[translated] = value
            for key, val in to_set:
                self.settings[key] = val

            for key in Ipsec2Swanctl.Child.settings_map:
                try:
                    del self.settings[key]
                except:
                    pass
            auth_section = Ipsec2Swanctl.AuthSection(self.settings)
            if auth_section.auth_settings:
                self.auth_section = auth_section
            self.cleanup(self.settings)

        @classmethod
        def cleanup(cls, settings):
            lst = []
            lst.extend(Ipsec2Swanctl.auth_dict.keys())
            lst.extend(Ipsec2Swanctl.auth_dict.values())
            lst.extend(Ipsec2Swanctl.Child.settings_map.keys())
            lst.extend(["auto", "type", "compress", "rightallowany"])
            for key in lst:
                try:
                    del settings[key]
                except:
                    pass
    class Child():

        value_map = {
            "add" : "none",
            "route" : "trap",
            "start" : "start"
        }
        settings_map = {
            "compress" : ("compress", None),
            "auto" : ("start_action", value_map),
            "esp" : ("esp_proposals", None),
            "ah" : ("ah_proposals", None),
            "sha256_96" : ("sha256_96", None),
            "leftsubnet" : ("local_ts", None),
            "rightsubnet" : ("remote_ts", None),
            "installpolicy" : ("policies", None),
            "dpdaction" : ("dpd_action", None),
            "closeaction" : ("close_action", None)
        }
        def __init__(self, name, settings):
            self.ignored = True
            self.name = name
            self.settings = settings
            self.check_if_ignored()
            self.transform_proposals()
            self.convert_settings(self.settings)

        def __repr__(self):
            return self.to_swanctl()

        def check_if_ignored(self):
            if self.settings.get("auto") != "ignore":
                self.ignored = False

        def transform_proposals(self):
            for item in ("esp", "ah"):
                existing_val = self.settings.get(item)
                if existing_val:
                    existing_val = existing_val.strip()
                    if not existing_val.endswith("!"):
                        existing_val = existing_val + ", default"
                        self.settings[item] = existing_val

        @classmethod
        def convert_settings(cls, settings):
            to_replace = {}
            for key, val in cls.settings_map.items():
                existing_val = settings.get(key)
                if existing_val:
                    to_replace[key] = val[0]
                    if val[1]:
                        translation_value = val[1].get(existing_val)
                        if translation_value:
                            settings[key] = translation_value
            for key, val in to_replace.items():
                settings[val] = settings[key]
                del settings[key]

        def to_swanctl(self, defaults_path=None):
            ret = "%s {\n" % self.name
            if defaults_path:
                ret += "\tinclude %s\n" % defaults_path
            for key, val in self.settings.items():
                ret += "\t%s=%s\n" % (key, val)
            ret += "}\n"
            if self.ignored:
                new_ret = ""
                for line in ret.splitlines():
                    new_ret +="# %s\n" % line
                ret = new_ret
            return ret

    class Authority():
        val_map = {
            "cacert" : "cacert",
            "crluris" : "crl_uris",
            "ocspuris" : "ocsp_uris",
            "certuribase" : "cert_uri_base"
        }
        def __init__(self, name, settings):
            self.name = name
            for item in ("cacert", "crluris", "ocspuris", "certuribase"):
                val = settings.get(item)
                constructed_val = None
                if val:
                    if isinstance(val, (list, dict)):
                        constructed_val = ", ".join(val)
                    else:
                        constructed_val = val
                setattr(self, item, constructed_val)

        def to_swanctl(self):
            ret = "%s {\n" % self.name
            for item in self.val_map:
                mapped = self.val_map[item]
                val = getattr(self, item, None)
                if val:
                    ret += "\t%s=%s\n" % (mapped, val)
            ret += "}\n"
            return ret

    def __init__(self):
        self.args = None
        self.config_setup = None
        self.ca_sections = None
        self.conn_sections = None
        self.conn_default = None
        self.includes = None
        self.ike_configs_are_the_same = None

    @classmethod
    def main(cls):
        """"

        """
        argument_parser = argparse.ArgumentParser()
        argument_parser.add_argument(
            "--sysconfdir",
            help="sysconfdir (directory under which ipsec.conf and ipsec.secrets can be found)",
            default="/etc")
        argument_parser.add_argument(
            "--ipsecconf",
            help="path to ipsec.conf to translate. If unset, defaults to ipsec.conf "
            "relative to sysconfdir. If set, does not write out default files for IKE_SA "
            "and CHILD_SA configs",
            default="")
        argument_parser.add_argument(
            "-o", "--output",
            help="output file name to write swanctl.conf syntax to",
            default="swanctl.conf")
        argument_parser.add_argument(
            "-w", "--write",
            action="store_true",
            help="Whether to write the output file or not")
        argument_parser.add_argument(
            "-d", "--debug",
            action="store_true",
            help="Whether to enable debug logging"
            )
        class_instance = Ipsec2Swanctl()
        class_instance.args = argument_parser.parse_args()
        class_instance.args.sysconfdir = class_instance.args.sysconfdir.rstrip("/")
        if class_instance.args.debug:
            logging.basicConfig(level=logging.DEBUG)
        else:
            logging.basicConfig(level=logging.WARN)
        class_instance.readipsecconf()
        class_instance.readipsecsecrets()
        gathered = {}
        for name, conn in class_instance.conn_sections.items():
            secrets = cls.find_matching_secrets(
                class_instance.conn_sections, class_instance.secrets, name)
            gathered[name] = (conn, secrets)

        logging.debug("gathered: %s", gathered)

        collapsed_conns, merged = class_instance.expand_and_collapse_also_structures(
            class_instance.conn_sections)
        # now all other conns need to be processed
        to_skip = []
        to_skip.extend(collapsed_conns)
        to_skip.extend(merged)
        processed = []
        processed.extend(collapsed_conns.values())
        for name in class_instance.conn_sections:
            if name not in to_skip:
                logging.info("Transforming %s", name)
                transformed = Ipsec2Swanctl.Connection(name,
                    class_instance.conn_sections[name], {})
                child_config = class_instance.make_child_config(name,
                    class_instance.conn_sections[name])
                class_instance.integrate_child(name, transformed, child_config)
                processed.append(transformed)
                secrets = gathered[name][1]

        logging.debug(processed)

        # transform the rest into Connection structures

        # generate strings that are the objects in swanctl config
        generated_file_contents = class_instance.translate(processed)
        if class_instance.args.write:
            class_instance.writeout_file(generated_file_contents)
        if not class_instance.args.ipsecconf:
            class_instance.generate_default_swanctl_config()

    def writeout_file(self, file_contents):
        with open(self.args.output, "w") as f:
            f.write(file_contents)

    def readipsecconf(self):
        """
        Reads ipsec.conf
        """
        config_setup = {}
        # maps conn names to their settings
        conn_sections = collections.OrderedDict()
        conn_default = {}
        ca_sections = {}
        # current_section contains the keys in ipsec.conf syntax with their values
        current_section = {}
        in_config_setup = False
        conn_name = None
        self.includes = {}
        includes = self.includes
        ipsecconfpath = self.args.ipsecconf if self.args.ipsecconf != "" \
        else self.args.sysconfdir + "/ipsec.conf"
        with open(ipsecconfpath, "r", encoding='utf-8',
            errors='ignore') as ipsecconf:
            for index, line in enumerate(ipsecconf.readlines(), start=1):
                splits = None
                stripped = line.strip().rstrip()
                if '=' in stripped:
                    splits = stripped.split(sep="=", maxsplit=1)
                    for index2, split in enumerate(splits):
                        splits[index2] = split.strip().rstrip()
                else:
                    splits = stripped.split()
                logging.debug(splits)
                if splits and len(splits) > 1:
                    if splits[0] == 'conn':
                        conn_name = splits[1]
                        in_config_setup = False
                        if conn_name == "%default":
                            current_section = conn_default
                        else:
                            current_section = {}
                            conn_sections[conn_name] = current_section
                            includes[conn_name] = []
                    elif splits[0] == 'config' and splits[1] == 'setup':
                        in_config_setup = True
                    elif splits[0] == 'ca':
                        ca_name = splits[1]
                        in_config_setup = False
                        current_section = {}
                        ca_sections[ca_name] = current_section
                        includes[ca_name] = []
                    else:
                        try:
                            if in_config_setup:
                                config_setup[splits[0]] = splits[1]
                            else:
                                if splits[0] == "also":
                                    includes[conn_name].append(splits[1])
                                else:
                                    current_section[splits[0]] = splits[1]
                        except IndexError:
                            logging.warning("Malformed line %s \"%s\"; Line ignored.",
                                index, line, file=sys.stderr)
        self.config_setup = config_setup
        self.ca_sections = ca_sections
        self.conn_sections = conn_sections
        self.conn_default = conn_default
        # print(self.config_setup)
        #print(self.conn_sections)
        for section in self.conn_sections.values():
            self.apply_deprecate_settings(section)
        #self.expand_and_collapse_also_structures(self.conn_sections)
        #print(self.conn_sections)
        #print(len(self.conn_sections))

    def write_default_config_files(self, ike_sa_file_contents, child_sa_file_contents):
        ike_sa_conf_path = self.generate_ike_sa_conf_path()
        child_sa_conf_path = self.generate_child_sa_conf_path()

        with open(ike_sa_conf_path, "w") as file:
            file.write(ike_sa_file_contents)
        with open(child_sa_conf_path, "w") as file:
            file.write(child_sa_file_contents)

    def generate_default_swanctl_config(self):
        """
        Creates two files that store the IKE_SA and CHILD_SA default configs
        which are generated by parsing conn %default
        """
        def generate_file_contents(name, settings):
            ret="# Global defaults for %s settings; Applied using include mechanism\n" % name
            for key, val in settings.items():
                ret += "%s=%s\n" % (key, val)
            return ret

        auth_section = Ipsec2Swanctl.AuthSection(self.conn_default)

        # remove child_sa keys from ike_sa config and ike_sa keys from child_sa config
        #for key in Ipsec2Swanctl.Child.settings_map:
        #    try:
        #        del ike_sa_settings[key]
        #    except KeyError:
        #        pass

        #for key in Ipsec2Swanctl.Connection.settings_map:
        #    try:
        #        del child_sa_settings[key]
        #    except KeyError:
        #        pass

        translated_settings = {}
        for key, val in self.conn_default.items():
            translated_key = Ipsec2Swanctl.Connection.settings_map.get(key)
            if translated_key:
                translated_settings[key]=val

        Ipsec2Swanctl.Connection.convert_settings(translated_settings)
        ike_sa_settings = translated_settings

        translated_settings = {}
        for key, val in self.conn_default.items():
            translated_key = Ipsec2Swanctl.Child.settings_map.get(key)
            if translated_key:
                translated_settings[key]=val
        
        Ipsec2Swanctl.Child.convert_settings(translated_settings)

        child_sa_settings = translated_settings


        ike_sa_file_contents = generate_file_contents("IKE_SA", ike_sa_settings)
        child_sa_file_contents = generate_file_contents("CHILD_SA", child_sa_settings)
        ike_sa_file_contents += "\n"
        ike_sa_file_contents += auth_section.to_swanctl()

        os.makedirs(name=self.generate_swanctl_conf_d_path(), mode=0o770, exist_ok=True)
        self.write_default_config_files(ike_sa_file_contents, child_sa_file_contents)

    def generate_swanctl_conf_d_path(self):
        """
        Generates the path to the swanctl/conf.d directory
        """
        return self.args.sysconfdir + "/swanctl/conf.d/"

    def generate_ike_sa_conf_path(self):
        """
        Generates and returns the path to the default config file for IKE_SAs
        """
        return self.generate_swanctl_conf_d_path() + "ike_sa_default.conf"

    def generate_child_sa_conf_path(self):
        """
        Generates and returns the path to the default config file for CHILD_SAs
        """
        return self.generate_swanctl_conf_d_path() + "child_sa_default.conf"

    def expand_and_collapse_also_structures(self, all_conns):
        """
        first expands and then collapses all includes using "also" in order to find which settings
        can be shared by using a common conn# and hwich need their own conns,
        also to find which make their own children and which don't
        """
        def process_include(conn_name, all_conns, already_processing):
            this_conn = all_conns[conn_name]
            includes = self.includes[conn_name]
            if conn_name in already_processing:
                logging.info("Already processing %s, encountered recursion.",
                    conn_name, file=sys.stderr)
                return {}
            already_processing.append(conn_name)

            if includes:
                for include in includes:
                    logging.info("Processing include %s for conn %s", include, conn_name)
                    returned_dict = process_include(include, all_conns, already_processing)
                    dict_copy = copy.copy(returned_dict)
                    #print("dict_copy: %s" % dict_copy)
                    #print("this_conn: %s" % this_conn)
                    dict_copy.update(this_conn)
                    #print("dict_copy after update: %s" % dict_copy)
                    this_conn = dict_copy
                    all_conns[conn_name] = this_conn
            return this_conn
        def compare_conns(conn1_name, conn1, conn2_name, conn2):
            # just very weak comparisons, not all that are necessary for all supported features
            for key in ["leftauth", "rightauth", "left", "right", "modeconfig", "leftid", "rightid",
                        "leftsendcert", "keyingtries", "mobike", "rekey_time", "lifetime",
                        "ikelifetime", "randtime", "dpdaction", "closeaction", "aggressive"]:
                value1 = conn1.get(key)
                value2 = conn2.get(key)
                if value1 != value2:
                    return False
            return True

        def store_associations(conn1_name, conn2_name, ike_configs_match):
            if not isinstance(ike_configs_match.get(conn1_name), list):
                ike_configs_match[conn1_name] = []
            if not isinstance(ike_configs_match.get(conn2_name), list):
                ike_configs_match[conn2_name] = []
                if conn2_name not in ike_configs_match[conn1_name]:
                    ike_configs_match[conn1_name].append(conn2_name)
                if conn1_name not in ike_configs_match[conn2_name]:
                    ike_configs_match[conn2_name].append(conn1_name)


        # identify all conns that are included and build include chains
        # recursively include all conns
        for conn_name, includes in self.includes.items():
            already_processing = []
            logging.info("Processing conn %s", conn_name)
            for item in includes:
                logging.info("Processing include %s", item)
                if len(self.includes[conn_name]) > 0:
                    self.conn_sections[conn_name].update(process_include(
                        conn_name, all_conns, already_processing))

        # move all the conns that do not have includes in front
        no_includes = collections.OrderedDict()
        for conn_name, settings in all_conns.items():
            if conn_name not in self.includes:
                no_includes[conn_name] = settings
        for conn_name, settings in all_conns.items():
            if conn_name in self.includes:
                no_includes[conn_name] = settings
        
        logging.info("Configs without any includes: %s", no_includes)
        all_conns=no_includes
        
        # check for duplicate ike configs and collapse those
        ike_configs_are_the_same = collections.OrderedDict()
        for conn1_name, conn1 in all_conns.items():
            for conn2_name, conn2 in all_conns.items():
                if conn1_name == conn2_name:
                    continue
                if compare_conns(conn1_name, conn1, conn2_name, conn2):
                    store_associations(conn1_name, conn2_name, ike_configs_are_the_same)
        # cleanup so one parent remains
        to_delete=[]
        for parent, children in ike_configs_are_the_same.items():
            if parent not in to_delete:
                for child in children:
                    to_delete.append(child)
        for parent in to_delete:
            try:
                del ike_configs_are_the_same[parent]
            except:
                pass
        logging.info("Following configs are the same and could be merged: %s", 
            ike_configs_are_the_same)
        self.ike_configs_are_the_same = ike_configs_are_the_same

        # collapse conns
        collapsed_conns = collections.OrderedDict()
        for name, same_conns in ike_configs_are_the_same.items():
            logging.debug("Making conn for %s", name)
            parent = Ipsec2Swanctl.Connection(name, all_conns[name], {})
            self.integrate_child(name, parent, self.make_child_config(name, all_conns[name]))
            logging.debug("same_conns of %s: %s", name, same_conns)
            for same_conn_name in same_conns:
                logging.info("Working on %s", same_conn_name)
                same_conn = self.conn_sections[same_conn_name]
                #print("Same_conn: %s" % same_conn)
                child_config = self.make_child_config(same_conn_name, same_conn)
                #print("Child_config: %s" % child_config)
                self.integrate_child(same_conn_name, parent, child_config)
            collapsed_conns[name] = parent
        #print("collapsed_conns: %s" % collapsed_conns)
        return collapsed_conns, to_delete

    @classmethod
    def integrate_child(cls, name, parent: Connection, child_config: Child):
        """
        Integrate the child config into the parent
        """
        parent.children[name] = child_config
    @classmethod
    def make_child_config(cls, conn_name, conn):
        """
        Gets all child config relevant settings and returns them in a child object
        """
        child_config = {}

        for setting in conn:
            if setting in cls.Child.settings_map:
                child_config[setting] = conn[setting]
        child_as_class = cls.Child(conn_name, child_config)
        child_as_class.name = conn_name
        return child_as_class

    @classmethod
    def generate_conn_name(cls, conn_name):
        return conn_name

    @classmethod
    def generate_child_name(cls, conn_name, child):
        return "%s-child-%s-%s" % (conn_name, child["leftsubnet"], child["rightsubnet"])

    @classmethod
    def split_out_parents_and_children(cls, conn_name, conns_with_secrets):
        """
        Generate the settings for one conn
        """

        def translate_settings(name, conn, secrets):
            pass

        def generate_connections(name, conn):
            pass

        def generate_secrets(name, secrets):
            generated_content = "secrets {\n"
            for item in secrets:
                generated_content += item.to_swanctl()
            generated_content += "}\n"

        # put out all conns with their children and secrets
        for conn, secrets in conns_with_secrets:
            conn_swanctl = conn_to_swanctl(conn)
            secrets_swanctl = "secrets {\n"
            suffix=0
            for secret in secrets:
                secrets_swanctl += secret.to_swanctl(suffix)
                suffix +=1
            secrets_swanctl += "}\n"
            if self.args.split:
                new_filename = cls.generate_filename(conn)
                cls.writeout_file(new_filename, conn_swanctl + "\n" + secrets_swanctl)

    @classmethod
    def __get_includes(cls, include_lines : list):
        includes = []
        for item in includes_lines:
            includes.extend(glob.glob(item))
        return includes

    @classmethod
    def __process_secrets_includes(cls, includes_list):
        for file in includes_list:
            processed_contents = self.process_secrets_file(file)
            self.add_new_content(file, processed_contents)

    @classmethod
    def __process_ipsecconf_includes(cls, includes_list):
        for file in includes_list:
            processed_contents = self.process_ipsecconf_file(file)
            cls.add_new_content(file, processed_contents)


    @classmethod
    def find_matching_secrets(cls, conns, all_secrets, conn_name):
        def determine_auth_type(conn, all_secrets):
            return "PSK"
        """
        Find the matching secrets for a connection
        """
        this_conn = conns[conn_name]
        ids = cls.__getids(this_conn)
        auth_type = determine_auth_type(this_conn, all_secrets)

        return cls.find_matching_secret(auth_type, all_secrets, ids,
            cls.__get_addresses(conns[conn_name]))

    @classmethod
    def __get_addresses(cls, conn):
        return conn.get("left", "%any"), conn.get("right", "%any")
    @classmethod
    def __getids(cls, conn):
        return conn.get("leftid", conn.get("left")), conn.get("rightid", conn.get("right"))


    @classmethod
    def find_matching_secret(cls, auth_type, all_secrets, ids, addresses):
        auth2types = {
        "PSK" : "PSK",
        "psk" : "psk",
        "pubkey" : "RSA"
        }
        matches = []
        for secret in all_secrets:
            for identity in ids:
                for secret_identity in secret.selectors:
                    if identity == secret_identity or secret_identity == "" and \
                auth2types[auth_type] == secret.secrets_type:
                        matches.append(secret)
            for address in addresses:
                for secret_identity in secret.selectors:
                    if address == secret_identity:
                        matches.append(secret)
        return matches

    @classmethod
    def apply_deprecate_settings(cls, section: dict):
        """
        Converts settings such as authby into leftauth and rightauth
        Also applies uniqueids=foo to all conns, if set
        """
        def get_auth_value(translated_authby_value, authentication_round: str):
            return translated_authby_value.get(authentication_round)

        value_map_authby = {
            "pubkey" : { 
                "leftauth" :"pubkey",
                "rightauth" : "pubkey"
            },
            "psk" : {
                "leftauth" : "psk",
                "rightauth" : "psk",
            },
            "rsasig" : { 
                "leftauth" :"pubkey",
                "rightauth" : "pubkey"
            },
            "ecdsasig" : { 
                "leftauth" :"pubkey",
                "rightauth" : "pubkey"
            },
            "secret" : { 
                "leftauth" :"psk",
                "rightauth" : "psk"
            },
            "xauthpsk" : {
                "leftauth" : "psk",
                "rightauth" : "psk",
                "rightauth2" : "xauth-generic"
            },
            "xauthrsasig" : {
                "leftauth" : "pubkey",
                "rightauth" : "pubkey",
                "rightauth2" : "xauth-generic"
            }
        }
        value_map = {
            "authby" : value_map_authby
        }
        
        key=section.get("authby")
        if key:
            for val in ("leftauth", "rightauth", "leftauth2", "rightauth2"):
                val2 = get_auth_value(value_map["authby"][section["authby"]], val)
                if val2:
                    section.setdefault(val, val2)
            del section["authby"]
        section.setdefault("auto", "ignore")

        key=section.get("leftprotoport")
        if key:
            # get default
            section.setdefault("leftsubnet", "%dynamic")
            section["leftsubnet"] = str.join(", ", map(lambda x: (x + "[%s]" % section.get("leftprotoport")),
                section["leftsubnet"].split(",")))
            del section["leftprotoport"]
        key=section.get("rightprotoport")
        if key:
            # get default
            section.setdefault("rightsubnet", "%dynamic")
            section["rightsubnet"] = str.join(", ", map(lambda x: (x + "[%s]" % section.get("rightprotoport")),
                section["rightsubnet"].split(",")))
            del section["rightprotoport"]

    def readipsecsecrets(self):
        """
        Foobar
        """
        self.secrets = []
        with open(self.args.sysconfdir + "/ipsec.secrets", "r", encoding='utf-8', errors='ignore') as f:
            for line in f.readlines():
                try:
                    line = line.strip()
                    if line.startswith("#"):
                        continue
                    selector, rest = line.split(":", maxsplit=1)
                    stuff = rest.strip().split(maxsplit=1)

                    secrets_type = stuff[0]
                    rest = None
                    if len(stuff) == 2:
                        rest = stuff[1]
                    self.secrets.append(self.Secret(selector, secrets_type, rest))
                except ValueError:
                    logging.warning("Failed to read in line %s, incorrect format.", line.strip())
    @classmethod
    def generate_ca(cls, name, ca_section: dict):
        cacert = ca_section.get("cacert")
        crluris = [ ca_section.get("crluri", ca_section.get("crluri1")) + ca_section.get("crluri2") ]
        ocspuris = [ ca_section.get("ocspuri", ca_section.get("ocspuri1")) + ca_section.get("ocspuri2") ]
        certuribase = ca_section.get("certuribase")
        to_pass = {}
        for item in ((cacert, "cacert"), (crluris, "crluris"), (ocspuris, "ocspuris"), (certuribase, "certuribase")):
            if item[0]:
                to_pass[item[1]] = item[0]

        return Ipsec2Swanctl.Authority(
            name,
            to_pass
            )

    def translate(self, processed: Connection):
        def generate_pool(conn: dict):
            pools = conn.get("pools")
            dns_servers = conn.get("rightdns")
            pool_addresses = []
            pool_dns_servers = []
            if pools:
                pool_addresses.append(pools)
            if dns_servers:
                pool_dns_servers.append(dns_servers)
                del conn["rightdns"]
            if dns_servers or pools:
                return Ipsec2Swanctl.Pool(pool_addresses, pool_dns_servers)
            return None

        def find_matching_pool(all_pools : list, settings: dict):
            settings_ip = settings["pools"]
            for pool in all_pools:
                if pool.subnet == settings_ip:
                    return pool
            return None
        def fill_in_pool(pool, settings : dict):
            """
            fill in the pool name in rightsourceip
            so it'S later correctly replaced with pools=pool_name
            """
            try:
                del settings["rightdns"]
            except:
                pass
            settings["pools"] = pool.get_identifier()
            return True

        # extract pools and later fill in names
        ret="connections {\n"
        authorities = []
        for name, auth in self.ca_sections.items():
            logging.debug(name, auth)
            authority = Ipsec2Swanctl.Authority(name, auth)
            authorities.append(authority)
        pools=[]
        for connection in processed:
            if "pools" in connection.settings:
                existing_pool = find_matching_pool(pools, connection.settings)
                if not existing_pool:
                    pool=generate_pool(connection.settings)
                    if pool:
                        pools.append(pool)
                fill_in_pool(pool, connection.settings)
            swanctl_syntax = connection.to_swanctl(
                ike_sa_defaults_path=self.generate_ike_sa_conf_path(),
                child_sa_defaults_path=self.generate_child_sa_conf_path())
            for line in swanctl_syntax.splitlines():
                if connection.ignored:
                    ret += "#"
                ret += "\t%s\n" % line
        ret += "}\npools {\n"
        for pool in pools:
            swanctl_syntax = pool.to_swanctl()
            for line in swanctl_syntax.splitlines():
                ret += "\t%s\n" % line
        ret += "}\nauthorities {\n"
        for authority in authorities:
            # translate to swanctl
            swanctl_syntax = authority.to_swanctl()
            for line in swanctl_syntax.splitlines():
                ret += "\t%s\n" % line
        ret += "}\nsecrets {\n"
        for secret in self.secrets:
            swanctl_syntax = secret.to_swanctl()
            for line in swanctl_syntax.splitlines():
                ret += "\t%s\n" % line            
        ret += "}\n"
        return ret
    @classmethod
    def generate_filename(cls, name):
        return "%s.conf" % name

if __name__ == '__main__':
    Ipsec2Swanctl.main()
